public class Dog{
  String name;
  String breed;
  int age;

  public Dog(String name, String breed, int age){
    this.name = name;
    this.breed = breed;
    this.age = age;
  }
  public String bark(){
    return "WOOF!";
  }
  public String sayHi(){
    return "Howdy, my name is " + name + " I am a " + age + " year old " + breed; 
  }
}