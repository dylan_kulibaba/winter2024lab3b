import java.util.Scanner;
public class VirtualPetApp{
  public static void main(String[] args){
    Scanner read = new Scanner(System.in);
    Dog[] kennel = new Dog[4];
    for(int i = 0; i < kennel.length; i++){
      System.out.println("\nWhat is the name of your dog");
      String name = read.nextLine();
      System.out.println("\nHow old is " + name);
      int age = read.nextInt();
      read.nextLine();
      System.out.println("\nWhat breed is " + name);
      String breed = read.nextLine();
      kennel[i] = new Dog(name, breed, age);
    }
    System.out.println("\n" + kennel[3].name + " " +kennel[3].age + " " + kennel[3].breed + "\n");
    System.out.println(kennel[0].bark() + "\n" + kennel[0].sayHi());
  }
}